const faker = require('faker');

faker.locale = 'pt_BR';
const clients = [];

for (let index = 0; index < 500; index++){
    const name = faker.name.findName();
    const birt = faker.date.past(109,2019)
    const genre = faker.random.boolean() == true ? 'M' : 'F';
    const lastpurchase = faker.date.past();
    const countpurchase = faker.random.number(50);

    client = {
        name,
        birt,
        genre,
        lastpurchase,
        countpurchase,

    }
    clients.push(client);

}
//1-Desenvolva, utilizando filter , uma função que, dado um caractere de entrada, retorne todos os registros de clientes cujo o nome inicia com o caractere.
const inicialNome = letra => clientes.filter(value => value.name.charAt(0) == letra.toUpperCase());
console.log(inicialNome('x'));

//2-Retorne o array de clientes
const names = Clients.map(value => value)
console.log(names)

// 3-Desenvolva uma função que, dado o caractere de entrada, retorna apenas um número com o total de registros encontrados.
const quantLetraInicial = (pl) => Clients.map(value => value.name[0] === pl)
  .reduce((acumulador, next) => (acumulador += next));
console.log(quantLetraInicial("A"))

//4-Desenvolva uma função que retorne apenas os nomes dos clientes.
const clientes = Clients.map(value => value.name)
console.log(clientes)

//5-Desenvolva uma função que retorne apenas o primeiro nome dos clientes.
let firstName = Clients.map(value => value.name.split(" ")[0])
console.log(firstName)

//11-Implemente uma função que retorne os dados dos clientes que já realizaram mais de 15 compras.
const more = clients.filter(el => el.countpurchase > 15)
console.log(more);








